/**
 * Copyright (c) 2010-2011 Encori Tomasz Wojewódka All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package pl.encori.liferay.fix.lps17381;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.service.AddressLocalServiceUtil;

/**
 * Listens for address model changes and removes the number only if value of
 * "Street 1" field equals "0".
 * 
 * @author Tomasz Wojewódka
 */
public class AddressListener extends BaseModelListener<Address> {
	public void onAfterCreate(Address Address) throws ModelListenerException {

	}

	public void onAfterRemove(Address Address) throws ModelListenerException {
		regenerateAddress(Address, "onAfterRemove");

	}

	public void onAfterUpdate(Address Address) throws ModelListenerException {

	}

	public void onBeforeCreate(Address Address) throws ModelListenerException {

	}

	public void onBeforeRemove(Address Address) throws ModelListenerException {

	}

	public void onBeforeUpdate(Address Address) throws ModelListenerException {

	}

	/**
	 * Removes address only when field "Street 1" equals "0"
	 * 
	 * @param address
	 * @param msg
	 */
	private void regenerateAddress(Address address, String msg) {

		if (address.getStreet1().equals("0")) {
			_log.info("Removing address: " + address.toString());
		} else {

			try {
				AddressLocalServiceUtil.updateAddress(address);
				_log.info("Handling bug LPS-17381: " + address.toString());
			} catch (SystemException e) {
				_log.info("Exception during handling bug LPS-17381 : "
						+ address.toString());
				e.printStackTrace();
			}
		}

	}

	private static Log _log = LogFactoryUtil.getLog(AddressListener.class);

}
